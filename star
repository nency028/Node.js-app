stages:
  - build
  - deploy

variables:
  AWS_DEFAULT_REGION: $AWS_REGION
  AWS_REGISTRY: $ECR_REGISTRY
  ECR_REPOSITORY: node-ec2-ecr
  EC2_PRIVATE_KEY: $EC2_PRIVATE_KEY
  EC2_HOST: $EC2_HOST
  EC2_USERNAME: $EC2_USERNAME
  DOCKER_HOST: tcp://docker:2375

build:
  stage: build
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  services:
    - docker:dind
  before_script:
    - amazon-linux-extras install docker
    - aws --version
    - docker --version
  script:
    - docker build -t $AWS_REGISTRY/$ECR_REPOSITORY:$CI_COMMIT_SHA .
    - aws ecr get-login-password | docker login --username AWS --password-stdin $AWS_REGISTRY
    - docker push $AWS_REGISTRY/$ECR_REPOSITORY:$CI_COMMIT_SHA

deploy:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  services:
    - docker:dind
  before_script:
    - amazon-linux-extras install docker
    - aws --version
    - docker --version
    - yum update -y
    - yum install -y openssh-clients
  script:
    - echo "Preparing SSH and deploying to EC2 instance..."

    - echo "Pulling Docker image from Amazon ECR..."
    - aws ecr get-login-password | docker login --username AWS --password-stdin $AWS_REGISTRY
    - docker pull $AWS_REGISTRY/$ECR_REPOSITORY:$CI_COMMIT_SHA
    - docker images
    - echo "Running Docker image on EC2 instance..."

    
    - ssh -i $EC2_PRIVATE_KEY $EC2_USERNAME@$EC2_HOST 'echo Hello from CI/CD again!'
    - ssh -i $EC2_PRIVATE_KEY $EC2_USERNAME@$EC2_HOST "docker run -d -p 8080:8080 $AWS_REGISTRY/$ECR_REPOSITORY:$CI_COMMIT_SHA"

    - docker ps
    - echo "byyyyyyy"

    - ssh $EC2_USERNAME@$EC2_HOST 'echo Hello from CI/CD again!'
    - ssh $EC2_USERNAME@$EC2_HOST "docker run -d --name myapp $AWS_REGISTRY/$ECR_REPOSITORY:$CI_COMMIT_SHA"
    - docker ps
  only:
    - main
